<h1>Following</h1>

<ul style="background-color:aliceblue;">
    @forelse(current_user()->follows as $user)
    <li class="font-weight-bold text-lg mb-4 ">
        <div class="flex items-center">
           <a href="{{route('profile',$user)}}"> 
               
        @if($user->avatar=='')
        <img src="{{$user->avatar}}"
        style="width:60px;height:60px"
        
        />
         @else
            <img src="{{URL::asset('/images/default.png')}}" style="height: 40px" class="rounded-circle mr-4" />
            @endif            {{$user->name}}

</a>
        </div>
    </li>
    @empty
    <p>No Friends Yet</p>
    @endforelse
</ul>