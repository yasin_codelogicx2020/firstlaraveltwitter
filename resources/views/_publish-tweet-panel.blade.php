<div class="border border-primary">
    <form method="POST" action="/tweets" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <textarea class="form-control" placeholder="Tweet Your Post" id="exampleFormControlTextarea1" rows="3"
                name="tweetpost"></textarea>

                <input type="file" name="tweetavatar" id="tweetavatar"/>
        </div>

        <div class="d-flex justify-content-between px-4">
            @if (auth()->user()->avatar!=null)
            <img src="{{auth()->user()->avatar}}" style="height: 40px" class="rounded-circle mr-4" />
            @else
            <img src="{{URL::asset('storage/images/default.png')}}" style="height: 40px" class="rounded-circle mr-4" />
            @endif

            {{'@'.auth()->user()->username}}
            <button type="submit" class="btn btn-primary shadow">
                Tweet It
            </button>
        </div>

    </form>

    @error('tweetpost')
    <p class="text-danger">{{$message}}</p>
    @enderror
</div>