<form  action="{{$user->path()}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PATCH')

        <div class="form-group">
            <label for="name"></label>
            <input type="file" class="form-control" name="avatar"/>
            

            @error('name')
            <small id="helpId" class="form-text text-muted">{{$message}}</small>
            @enderror
        </div>


</form>