<x-app>
    <form  action="{{$user->path()}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PATCH')

        <div class="form-group">
            <label for="name"></label>
            <input type="text" class="form-control" name="name" id="" aria-describedby="helpId" placeholder="Name"
            value="{{$user->name}}"
            >

            @error('name')
            <small id="helpId" class="form-text text-muted">{{$message}}</small>
            @enderror
        </div>

        <!-- <div class="form-group">
            <label for="username"></label>
            <input type="text" class="form-control" name="username" id="" aria-describedby="helpId" placeholder="username"
            value="{{$user->username}}"

            >

            @error('username')
            <small id="helpId" class="form-text text-muted">{{$message}}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="email"></label>
            <input type="email" class="form-control" name="email" id="" aria-describedby="helpId" placeholder="email"
            value="{{$user->email}}"

            >

            @error('email')
            <small id="helpId" class="form-text text-muted">{{$message}}</small>
            @enderror
        </div> -->


        <div class="form-group">
          <label for="avatar">avatar</label>
          <input type="file" name="avatar" id="avatar" class="form-control">

          @error('avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
        </div>

        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

        <div class="form-group">
          <button class="btn-primary" type="submit" name="" id="" class="form-control" placeholder="" aria-describedby="helpId">
              Submit
          </button>


          <a href="{{$user->path()}}">Cancel</a>
        </div>
        <!-- @include('profiles/_updateavatarform') -->
    </form>
</x-app>

