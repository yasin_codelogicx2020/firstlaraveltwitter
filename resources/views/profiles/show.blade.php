<x-app>




    @section('content')
    <header class="mb-6">
        <img src="{{URL::asset('/images/bunny.jpg')}}" style="max-width: 100%" />

        <div class="d-flex justify-content-between px-4">
            <div>
                <h2>{{$user->name}}</h2>
                <span class="font-italic"> Joined {{$user->created_at->diffforHumans()}}
                </span>
            </div>
            <div class="m-4">

                @can ('edit',$user)
                <a href="{{$user->path('edit')}}" class="btn btn-outline-primary">
                    Edit Profile
                </a>
                @endcan
                <x-follow-button :user="$user">

                </x-follow-button>
            </div>
        </div>


        <div class="card">
            Description:
            Description DescriptionDescriptionDescriptionDescrip
            tionDescriptionDescriptionDescriptionDescri
            ptionDescriptionDescriptionDescriptionDescr
            iptionDescriptionDescriptionDescriptionDesc
            riptionDescriptio
           
        </div>


    </header>

    <hr>

    @include('_timeline',[
    'tweets'=>$tweets
    ])
</x-app>