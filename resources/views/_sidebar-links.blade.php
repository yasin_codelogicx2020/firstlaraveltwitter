

<ul>
    <li class="font-weight-bold text-lg mb-4 ">
        <a href="/tweets">Home</a>
    </li>
    <li class="font-weight-bold text-lg mb-4 ">
        <a href="/explore">Explore</a>
    </li><li class="font-weight-bold text-lg mb-4 ">
        <a href="/">Notifications</a>
    </li><li class="font-weight-bold text-lg mb-4 ">
        <a href="/">Messages</a>
 
    </li><li class="font-weight-bold text-lg mb-4 ">
        <a href="{{route('profile',auth()->user()->username)}}">Profile</a>
    </li><li class="font-weight-bold text-lg mb-4 ">

    <form method="POST" action='/logout'>
        @csrf
        <button type="submit" class="btn-primary outline">Logout</a>

</form>
   
    </li>
</ul>