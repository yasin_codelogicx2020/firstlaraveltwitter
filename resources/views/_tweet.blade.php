<div class="card my-4">
    <div class="card-body has-img-left">

        <div class="content">
            <div class="mb-2 flex">
                <img src="{{$tweet->user->avatar}}" class="rounded-circle mr-4" style="height:80px" />
                <a href="{{route('profile',$tweet->user)}}" class="author">by {{$tweet->user->name}}</a>
            </div>
            <span class="card-text description">
                {{$tweet->body}}
                

                @if($tweet->tweets_avatar!='')
                <div class="card-img">
                    <img class="rounded" src="{{'storage/'.$tweet->tweets_avatar}}"
                        alt="" style="max-width: 100%">
                </div>
                @endif
                <div class="meta d-flex m-2">


                <form method="POST" action="/tweets/{{$tweet->id}}/like">
                        @csrf
                    <span class="likes mr-2 mx-5 ">
                        <button
                            class="fas fa-thumbs-up {{$tweet->isLikedBy(current_user())?'text-primary':'text-muted'}}">
                            {{$tweet->likes?:0}}</button>
                    </span>
                </form>

                    <form method="POST" action="/tweets/{{$tweet->id}}/like">
                        @csrf
                        @method('DELETE')
                        <span class="comments mr-2 ">
                            <button
                                class="fas fa-thumbs-down {{$tweet->isDislikedBy(current_user())?'text-primary':'text-muted'}}">
                                {{$tweet->dislikes?:0}}</button>
                        </span>

                    </form>


                </div>
        </div>
    </div>
</div>