<x-app>
    <h3>Explore The Users</h3>
        @foreach($users as $user)
        <div class="d-flex my-5">

        @if($user->avatar=='')
        <img src="{{$user->avatar}}"
        style="width:60px;height:60px"
        
        />
         @else
            <img src="{{URL::asset('/images/default.png')}}" style="height: 40px" class="rounded-circle mr-4" />
            @endif

        

        <a href="/profile/{{$user->username}}">
        <div class="mx-4">

            <h4>{{'@'.$user->username}}</h4>
        </div>
</a>
        </div>

        @endforeach
</x-app>