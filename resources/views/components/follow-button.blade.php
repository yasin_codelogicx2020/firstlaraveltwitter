@if(current_user()->isNot($user))
<form method="POST" action="/profiles/{{$user->username}}/follow">
    @csrf
    <button type="submit" class="btn btn-primary shadow">


        {{current_user()->following($user)?'Unfollow Me':'Follow ME'}}
    </button>

</form>
@endif