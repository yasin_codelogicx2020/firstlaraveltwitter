<x-master>
<main class="py-4">
            <div class="container">


         @if (auth()->check())
                <div class="row">
                    <div class="col-sm-3">
                        @include('_sidebar-links')
                    </div>
                    @endif
                    
                    <div class="col-sm-6">
                      {{$slot}}

                    </div>
                    @if (auth()->check())

                    <div class="col-sm-3">
                        @include('_friends-list')
                    </div>
                    @endif

                </div>
            </div>
        </main>
</x-master>