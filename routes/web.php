<?php

use Illuminate\Support\Facades\Route;
use PhpParser\Node\Stmt\GroupUse;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::middleware('auth')->group(function(){
    Route::get('/', 'TweetController@index')->name('home');
    
    Route::get('/tweets','TweetController@index')->name('home');

    Route::post('/tweets','TweetController@store');

    Route::post('/tweets/{tweet}/like','TweetLikesController@store');
    Route::delete('/tweets/{tweet}/like','TweetLikesController@destroy');



    Route::post('/profiles/{user:username}/follow','FollowsController@store');
   
    Route::get('/profile/{user:username}/edit','ProfilesController@edit')
    ->middleware('can:edit,user');

    Route::patch('/profile/{user:username}','ProfilesController@update')->middleware('can:edit,user');



    Route::get('/explore','ExploresController@index');


});



Route::get('/profile/{user:username}','ProfilesController@show')->name('profile');


Route::get('/practice','PracticeModel@index');

Auth::routes();

