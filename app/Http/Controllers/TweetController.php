<?php

namespace App\Http\Controllers;

use App\Tweet;

class TweetController extends Controller
{
    //

    public function index()
    {

       
      
        return view('tweets.index',[
            'tweets'=>auth()->user()->timeline(),
        ]);
    }
    public function store()
    {
        //persist

       $attributes= request()->validate(['tweetpost'=>'required|max:255']);

    //    dd($attributes);
    $avatar='';
      if (request('tweetavatar')) {
            $avatar=request('tweetavatar')->store('avatars');
        }

        Tweet::create([
            'user_id'=>auth()->id(),
            'body'=>$attributes['tweetpost'],
            'tweets_avatar'=>$avatar,

        ]);

        return redirect()->route('home');
    }

}