<?php

namespace App\Http\Controllers;



use App\User;
use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
class ProfilesController extends Controller
{
    //

    public function show(User $user)
    {
        


        return view('profiles.show',[
            'user'=>$user,
            'tweets'=>$user->tweets()->
            withLikes()->paginate(5)
        ]);
    }

    public function edit(User $user)
    {

        // if($user->isNot(current_user()))
        // {
        //     abort(404);
        // }

        // $this->authorize('edit',$user);

        return view('profiles.edit',compact('user'));
    }

    public function update(User $user)
    {   

        // $user=User::find(1);



      $attributes=  request()->validate([
            
            'name' => ['required', 'string', 'max:255'],
          
            'avatar' => ['file'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if (request('avatar')) {
            $attributes['avatar']=request('avatar')->store('avatars');
        }
        $user->update($attributes);

        return redirect($user->path());
    }
}
