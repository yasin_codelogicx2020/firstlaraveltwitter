<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, Followable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'username', 'name', 'email', 'password','avatar',
    ];

    // protected $table = 'my_flights';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function timeline()
    {
        // return Tweet::where('user_id',$this->id)
        // ->latest()
        // ->get();

        //includes all of user tweeets and everyone they follow order by descending
        $friends = $this->follows()->pluck('id');

        // $ids->push($this->id);

        return Tweet::whereIn('user_id', $friends)
            ->orWhere('user_id', $this->id)
            ->withLikes()
            ->latest()
            ->paginate(5);

    }

    public function tweets()
    {
        //has many relation
        return $this->hasMany(Tweet::class);

    }

    public function getAvatarAttribute($value)
    {
        return asset('storage/'.$value);
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password']=bcrypt($value);
    }
    public function path($append = '')
    {
        $path = route('profile', $this->username);

        return $append ? "{$path}/{$append}" : $path;
    }


    public function likes() 
    {
         return $this->hasMany(Like::class); 
    }


    // public function getRouteKeyName()
    // {
    //     return 'name';
    // }

}
