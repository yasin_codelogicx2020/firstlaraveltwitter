<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


class Tweet extends Model
{

    use Likeable;
    //

    protected $guarded=[];

    
   public function User()
   {
       return $this->belongsTo(User::class);
   }

   //tweet can be liked

}